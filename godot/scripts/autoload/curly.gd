extends Node

onready var shapeshop:Node2D = load("res://objects/shapeshop.tscn").instance()
var shape_color:Color = Color.black

func _ready():
	add_child(shapeshop)
	shapeshop.visible = false

### SHAPE SHOP ###

# returns a Line2D
func circle( radius:float, definition:int = 32 ):
	if shapeshop == null:
		return null
	var c:Line2D = shapeshop.circle.duplicate()
	c.radius = radius
	c.definition = definition
	c.default_color = shape_color
	return c

# returns a Line2D
func rectangle( size:Vector2, splits:int = 0 ):
	if shapeshop == null:
		return null
	var r:Line2D = shapeshop.rectangle.duplicate()
	r.size = size
	r.splits = splits
	r.default_color = shape_color
	return r

# returns a Line2D
func triangle( size:Vector2, orientation:float = 0, center:int = 1 ):
	if shapeshop == null:
		return null
	var t:Line2D = shapeshop.triangle.duplicate()
	t.size = size
	t.orientation = orientation
	t.center = center
	t.default_color = shape_color
	return t

# returns a CheckBox
func anchor():
	if shapeshop == null:
		return null
	return shapeshop.anchor.duplicate()

# returns a Button
func anchor_button():
	if shapeshop == null:
		return null
	return shapeshop.anchor_button.duplicate()

### FUNCTIONS ###

func purge( n:Node, force_free:bool = true ):
	while n.get_child_count() > 0:
		var sn:Node = n.get_child(0)
		n.remove_child( sn )
		if force_free:
			sn.queue_free()

func anchor_sync( cb:CheckBox, bu:Button ):
	bu.text = str(cb.use_count)
	if cb.use_count > 0:
		bu.set( "custom_styles/normal", 				shapeshop.anchor_button.get("custom_styles/pressed") ) 
		bu.set( "custom_styles/hover", 					shapeshop.anchor_button.get("custom_styles/pressed") ) 
		bu.set( "custom_styles/pressed", 				shapeshop.anchor_button.get("custom_styles/normal") )
		bu.set( "custom_colors/font_color", 			shapeshop.anchor_button.get("custom_colors/font_color_pressed") ) 
		bu.set( "custom_colors/font_color_hover", 		shapeshop.anchor_button.get("custom_colors/font_color_pressed") ) 
		bu.set( "custom_colors/font_color_pressed", 	shapeshop.anchor_button.get("custom_colors/font_color") )
	else:
		bu.set( "custom_styles/normal", 				shapeshop.anchor_button.get("custom_styles/normal") ) 
		bu.set( "custom_styles/hover", 					shapeshop.anchor_button.get("custom_styles/normal") ) 
		bu.set( "custom_styles/pressed", 				shapeshop.anchor_button.get("custom_styles/pressed") )
		bu.set( "custom_colors/font_color", 			shapeshop.anchor_button.get("custom_colors/font_color") ) 
		bu.set( "custom_colors/font_color_hover", 		shapeshop.anchor_button.get("custom_colors/font_color") ) 
		bu.set( "custom_colors/font_color_pressed", 	shapeshop.anchor_button.get("custom_colors/font_color_pressed") )
