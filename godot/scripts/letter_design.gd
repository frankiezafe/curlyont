extends Node2D

enum CONNECTOR { NONE, LINE, ARC }

signal canvas_refresh

onready var curly:Node = get_node("/root/curly")

export (int,3,200) var definition:int = 32
export (int,2,20) var anchor_circle:int = 4
export (float,1,200) var radius:float = 30 setget set_radius
export (Color) var bg_color:Color = Color(0,0,0,1) setget set_bgcolor
export (float,0,20) var bg_width:float = 1
export (Color) var fg_color:Color = Color(1,1,1,1)
export (float,0,20) var fg_width:float = 4
export (bool) var generate:bool = false setget set_generate

export (bool) var toggle_anchors:bool = true
export (bool) var auto_link:bool = true
export (bool) var avoid_overlaps:bool = true

var offset:Array = [0,0,0,0]
var initialised:bool = false
var big_distance:float = 0
var small_distance:float = 0
var circles:Array = []
var anchors:Array = []
var sequence:Array = []
var connections:Array = []

func set_radius(f:float):
	radius = f
	big_distance = radius * 2 * sqrt(2)
	small_distance = radius * sqrt(2)
	if initialised:
		set_generate(true)

func set_bgcolor(c:Color):
	bg_color = c
	if initialised:
		for l in $bg.get_children():
			l.default_color = c

func create_circle(p:Vector2,r:float):
	var cdata:Dictionary = { 'position':p, 'radius':r }
	circles.append( cdata )
	var c:Line2D = curly.circle( r )
	$guides.add_child(c)
	c.position = p
	return cdata

func create_anchor(p:Vector2):
	# creation of button
	var bp:Vector2 = p - curly.shapeshop.anchor.rect_size * .5
	for a in anchors:
		if a.rect_position == bp:
			print("duplicated button")
			return
	var a:CheckBox = curly.anchor()
	$anchors.add_child(a)
	anchors.append(a)
	# storing id
	a.rect_position = bp
# warning-ignore:return_value_discarded
	a.connect( "anchor_clicked", self, "anchor_clicked" )

class button_sorter:
	static func sort_ascending(a, b):
		if a.rect_position.y == b.rect_position.y and a.rect_position.x < b.rect_position.x:
			return true
		elif a.rect_position.y < b.rect_position.y:
			return true
		return false

func set_generate(b:bool):
	generate = false
	if b:
		# cleanup
		anchors = []
		curly.purge( $guides )
		curly.purge( $anchors )
		# generation
		curly.shape_color = bg_color
		var size:Vector2 = Vector2( radius*4, radius*6 )
		var offss:Vector2 = size*-.5
		var boundary:Line2D = curly.rectangle( size )
		$guides.add_child(boundary)
		boundary.set_size( size )
		create_anchor( offss + Vector2(0,0) * size )
		create_anchor( offss + Vector2(1,0) * size )
		create_anchor( offss + Vector2(1,1) * size )
		create_anchor( offss + Vector2(0,1) * size )
		var agap = TAU / anchor_circle
		for i in range(0,2):
			var c:Dictionary = create_circle( Vector2( 0, offss.y + (i+1)*radius*2 ), radius * 2 )
			for j in range(0,anchor_circle):
				var a:float = agap * j
				create_anchor( c.position + Vector2(cos(a),sin(a)) * c.radius )
		for y in range(0,3):
			for x in range(0,2):
				var c:Dictionary = create_circle( Vector2( offss.x + (x*2+1)*radius, offss.y + (y*2+1)*radius ), radius )
				for j in range(0,anchor_circle):
					var a:float = agap * j
					create_anchor( c.position + Vector2(cos(a),sin(a)) * c.radius )
		
		# generate anchors ids
		var bnum:int = $anchors.get_child_count()
		anchors.sort_custom( button_sorter, "sort_ascending" )
		for i in range(0,bnum):
			var bi:CheckBox = anchors[i]
			bi.id = i

func same_circle(a:Vector2,b:Vector2):
	for c in circles:
		var da:float = (c.position-a).length()
		var db:float = (c.position-b).length()
		if abs(da-c.radius) < 1e-5 and abs(db-c.radius) < 1e-5:
			return c
	return null

func arc(l:Line2D,c:Dictionary,from:Vector2,to:Vector2):
	var agap:float = TAU / definition
	var da:Vector2 = (from-c.position).normalized() 
	var db:Vector2 = (to-c.position).normalized() 
	var a:float = da.angle()
	var b:float = a + da.angle_to(db)
	if b < a:
		a -= agap
		while a >= b:
			l.add_point( c.position + Vector2(cos(a),sin(a)) * c.radius )
			a -= agap
	else:
		a += agap
		while a <= b:
			l.add_point( c.position + Vector2(cos(a),sin(a)) * c.radius )
			a += agap

func remove_overlaps(until:Dictionary):
	var stop:int = 0
	var until_next = null
	for i in range(0,sequence.size()-1):
		if sequence[i] == until:
			stop = i
			until_next = sequence[i+1]
	if until_next == null:
		return
	for i in range(0,stop):
		var p:Vector2 = sequence[i].position
		if p.x == until.position.x:
			if p.y > until.position.y and p.y < until_next.position.y:
				until.connector = CONNECTOR.NONE
			elif p.y < until.position.y and p.y > until_next.position.y:
				until.connector = CONNECTOR.NONE
		elif p.y == until.position.y:
			if p.x > until.position.x and p.x < until_next.position.x:
				until.connector = CONNECTOR.NONE
			elif p.x < until.position.x and p.x > until_next.position.x:
				until.connector = CONNECTOR.NONE

func find_connection(a:Dictionary,b:Dictionary):
	var id0:int = a.node.id
	var id1:int = b.node.id
	if id0 > id1:
		var tmp:int = id0
		id0 = id1
		id1 = tmp
	for c in connections:
		if c[0] == id0 && c[1] == id1:
			return true
	connections.append([id0,id1])
	return false

func consolidate():
	connections = []
	var zombies:Array = []
	for i in range(1,sequence.size()):
		var prev = sequence[i-1]
		var curr = sequence[i]
		prev.node.use_count = 0
		curr.node.use_count = 0
		if prev.node == curr.node:
			# duplicate!
			zombies.append(prev)
			continue
		if prev.auto_link && auto_link:
			prev.circle = null
			# connection is already done!
			if find_connection( prev, curr ):
				prev.connector = CONNECTOR.NONE
				continue
			# checking alignement
			if prev.position.x == curr.position.x:
				prev.connector = CONNECTOR.LINE
			elif prev.position.y == curr.position.y:
				prev.connector = CONNECTOR.LINE
			else:
				# are anchors belonging to the same circle?
				var c = same_circle(prev.position, curr.position)
				if c != null:
					prev.connector = CONNECTOR.ARC
					prev.circle = c
				else:
					prev.connector = CONNECTOR.NONE
	for z in zombies:
		sequence.erase(z)
	for s in sequence:
		if s.connector == CONNECTOR.LINE && avoid_overlaps:
			remove_overlaps(s)
		s.node.pressed = true
		s.node.use_count += 1

func render():
	# generation of graphic
	while $render.get_child_count() > 0:
		var c:Line2D = $render.get_child(0)
		$render.remove_child(c)
	var l:Line2D = null
	for i in range(0,sequence.size()-1):
		var c:int = sequence[i].connector
		var curr:Vector2 = sequence[i].position
		var next:Vector2 = sequence[i+1].position
		if c == CONNECTOR.NONE:
			l = null
			continue
		else:
			if l == null:
				l = Line2D.new()
				l.width = fg_width
				l.default_color = fg_color
				l.joint_mode = Line2D.LINE_JOINT_ROUND
				$render.add_child( l )
				l.add_point( curr )
			if c == CONNECTOR.LINE:
				l.add_point( next )
			elif c == CONNECTOR.ARC:
				arc(l,sequence[i].circle,curr,next)
	if !Engine.editor_hint:
		VisualServer.force_draw()

func center():
	var vps:Vector2 = get_viewport().size - Vector2(offset[1]+offset[3],offset[0]+offset[2])
	position = vps * .5 + Vector2( offset[3], offset[0] )

func _ready():
	if !Engine.editor_hint:
		set_generate(true)
		initialised = true
		center()
# warning-ignore:return_value_discarded
		get_viewport().connect("size_changed",self,"center")

# warning-ignore:unused_argument
func _process(delta):
	if !initialised:
		set_generate(true)
		initialised = true
		if !Engine.editor_hint:
			VisualServer.render_loop_enabled = false

func _input(event):
	if Engine.editor_hint:
		return
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_BACKSPACE:
			for a in anchors:
				a.use_count = 0
				a.pressed = false
			sequence = []
			emit_signal("canvas_refresh")
			render()
		elif event.scancode == KEY_MINUS:
			if !sequence.empty():
				var s:Dictionary = sequence[sequence.size()-1]
				s.node.use_count -= 1
				if s.node.use_count == 0:
					s.node.pressed = false
				sequence.erase( s )
				if sequence.size() > 1:
					consolidate()
				emit_signal("canvas_refresh")
				render()

func get_anchor(id:int):
	var out = null
	for s in sequence:
		if s.node.id == id:
			out = s
	return out

func push_anchor(b:CheckBox):
	sequence.append({
		'node': b,
		'position': b.rect_position + b.rect_size*.5,
		'connector': CONNECTOR.NONE,
		'auto_link': true,
		'circle': null
	})
	if !auto_link && sequence.size() > 1:
		sequence[sequence.size()-2].auto_link = false

func anchor_clicked(id:int):
	var a:CheckBox = anchors[id]
	if toggle_anchors:
		var s = get_anchor(id)
		if s != null:
			sequence.erase(s)
			a.use_count -= 1
			if a.use_count == 0:
				a.pressed = false
		else:
			push_anchor(a)
			a.pressed = true
	else:
		a.pressed = true
		# avoiding multiple entry of the same button
		if !sequence.empty() and sequence[sequence.size()-1].node == a:
			return
		push_anchor(a)
	consolidate()
	render()

func anchor_clear(id:int):
	var do_consolidate:bool = false
	var a:CheckBox = anchors[id]
	var s = get_anchor(id)
	while(s != null):
		do_consolidate = true
		sequence.erase(s)
		s.node.use_count -= 1
		s = get_anchor(id)
	if do_consolidate:
		a.use_count = 0
		a.pressed = false
		consolidate()
		render()

func anchor_position(id:int):
	return anchors[id].rect_global_position + anchors[id].rect_size * .5
