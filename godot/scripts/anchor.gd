extends CheckBox

signal anchor_clicked
signal anchor_enter
signal anchor_leave

var id:int = -1
var use_count:int = 0

func _ready():
# warning-ignore:return_value_discarded
	self.connect("pressed",self,"click")
# warning-ignore:return_value_discarded
	self.connect("mouse_entered",self,"enter")
# warning-ignore:return_value_discarded
	self.connect("mouse_exited",self,"leave")

func click():
	emit_signal("anchor_clicked",id)

func enter():
	emit_signal("anchor_enter",id)

func leave():
	emit_signal("anchor_leave",id)
