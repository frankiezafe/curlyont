extends PanelContainer

signal selement_clicked
signal selement_linked
signal selement_enter
signal selement_leave

onready var normal:StyleBoxFlat = load("res://themes/sequence_element_normal.stylebox")
onready var hover:StyleBoxFlat = load("res://themes/sequence_element_hover.stylebox")

var id:int = -1
var index:int = -1

func _ready():
# warning-ignore:return_value_discarded
	$box/id.connect("pressed",self,"clicked")
# warning-ignore:return_value_discarded
	$box/link.connect("pressed",self,"linked")
# warning-ignore:return_value_discarded
	self.connect("mouse_entered",self,"enter")
# warning-ignore:return_value_discarded
	self.connect("mouse_exited",self,"leave")

func text(s:String):
	if s.length() < 2:
		s = '0'+s
	$box/id.text = s

func link(b:bool):
	$box/link.pressed = b

func clicked():
	emit_signal("selement_clicked",index)

func linked():
	emit_signal("selement_linked",index)

func enter():
	set( "custom_styles/panel", hover )
	$box/id.set( "custom_colors/font_color", Color.black )
	$box/id.set( "custom_colors/font_color_hover", Color.black )
	emit_signal("selement_enter",index)

func leave():
	set( "custom_styles/panel", normal )
	$box/id.set( "custom_colors/font_color", Color.white )
	$box/id.set( "custom_colors/font_color_hover", Color.white )
	emit_signal("selement_leave",index)
