tool

extends Line2D

export (int,0,200) var splits:int = 3 setget set_splits
export (Vector2) var size:Vector2 = Vector2(10,10) setget set_size
export (bool) var generate:bool = false setget set_generate

var initialised:bool = false

func set_splits(i:int):
	splits = i
	if initialised:
		set_generate(true)

func set_size(v2:Vector2):
	size = v2
	if initialised:
		set_generate(true)

func set_generate(b:bool):
	generate = false
	if b:
		clear_points()
		var offset:Vector2 = size * -.5
		var prev:Vector2 = Vector2.ZERO
		var dir:Vector2 = Vector2(1,0)
		var gap:float = 1.0/(splits+1)
		add_point(offset + prev * size)
		for c in range(0,4):
# warning-ignore:unused_variable
			for i in range(0,splits+1):
				var p:Vector2 = prev + dir*gap*size
				add_point(offset + p)
				prev = p
			match c:
				0:
					dir = Vector2(0,1)
				1:
					dir = Vector2(-1,0)
				2:
					dir = Vector2(0,-1)

func _ready():
	if !Engine.editor_hint:
		set_generate(true)
		initialised = true

# warning-ignore:unused_argument
func _process(delta):
	if !initialised:
		set_generate(true)
		initialised = true
