extends Button

signal button_anchor_clicked
signal button_anchor_enter
signal button_anchor_leave

var id:int = -1

func _ready():
# warning-ignore:return_value_discarded
	self.connect("pressed",self,"click")
# warning-ignore:return_value_discarded
	self.connect("mouse_entered",self,"enter")
# warning-ignore:return_value_discarded
	self.connect("mouse_exited",self,"leave")

func click():
	emit_signal("button_anchor_clicked",id)

func enter():
	emit_signal("button_anchor_enter",id)

func leave():
	emit_signal("button_anchor_leave",id)
