tool

extends Line2D

export (int,3,200) var definition:int = 32 setget set_definition
export (float,0,500) var radius:float = 50 setget set_radius
export (bool) var generate:bool = false setget set_generate

var initialised:bool = false

func set_definition(i:int):
	definition = i
	if initialised:
		set_generate(true)

func set_radius(f:float):
	radius = f
	if initialised:
		set_generate(true)

func set_generate(b:bool):
	generate = false
	if b:
		clear_points()
		var agap:float = TAU / definition
		for i in range(0,definition+1):
			var a:float = agap * i
			add_point( Vector2(cos(a),sin(a))*radius )
			
func _ready():
	if !Engine.editor_hint:
		set_generate(true)
		initialised = true

# warning-ignore:unused_argument
func _process(delta):
	if !initialised:
		set_generate(true)
		initialised = true
