extends PanelContainer

export (Color) var normal_color:Color = Color(1,0,0,1)
export (Color) var hover_color:Color = Color(1,0,1,1)

func _ready():
	normal()
# warning-ignore:return_value_discarded
	connect("mouse_entered",self,"hover")
# warning-ignore:return_value_discarded
	connect("mouse_exited",self,"normal")

func normal():
	get("custom_styles/panel").bg_color = normal_color

func hover():
	get("custom_styles/panel").bg_color = hover_color
