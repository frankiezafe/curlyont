tool

extends Line2D

enum TRIANGLE_CENTER { CENTER, BARYCENTER, TOP, BACK }

export (Vector2) var size:Vector2 = Vector2(10,10) setget set_size
export (float,0,360) var orientation:float = 0 setget set_orientation
export (TRIANGLE_CENTER) var center:int = 0 setget set_center
export (bool) var generate:bool = false setget set_generate

var initialised:bool = false

func set_orientation(f:float):
	orientation = f
	if initialised:
		set_generate(true)

func set_size(v2:Vector2):
	size = v2
	if initialised:
		set_generate(true)

func set_center(i:int):
	center = i
	if initialised:
		set_generate(true)

func set_generate(b:bool):
	generate = false
	if b:
		clear_points()
		var rad:float = orientation / 180 * PI
		var pts:Array = []
		pts.append(Vector2(size.x * .5,0).rotated(rad))
		pts.append(Vector2(-size.x * .5,-size.y * .5).rotated(rad))
		pts.append(Vector2(-size.x * .5,size.y * .5).rotated(rad))
		pts.append(Vector2(size.x * .5,0).rotated(rad))
		var offset:Vector2 = Vector2.ZERO
		match center:
			TRIANGLE_CENTER.BARYCENTER:
				for i in range(0,3):
					offset -= pts[i]
				offset /= 3
			TRIANGLE_CENTER.TOP:
				offset = pts[1] + (pts[2]-pts[1]) * .5
			TRIANGLE_CENTER.BACK:
				offset = pts[0]
		for p in pts:
			add_point( offset + p )

func _ready():
	if !Engine.editor_hint:
		set_generate(true)
		initialised = true

# warning-ignore:unused_argument
func _process(delta):
	if !initialised:
		set_generate(true)
		initialised = true
