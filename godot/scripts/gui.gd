extends Node2D

onready var curly:Node = get_node("/root/curly")

export (NodePath) var _canvas:NodePath = ""

onready var canvas = get_node(_canvas)
onready var selement_tmpl:PanelContainer = $right/rows/sequence.get_child(0).duplicate()
var anchor_tmpls:Array = []
var adjust_request:bool = false

func adjust_screen():
	var vps:Vector2 = get_viewport().size
	$right.rect_position = (vps - $right.rect_size) * Vector2.RIGHT
	$right/anchor_link.visible = false

func synch():

	if canvas != null:
		canvas.connect("canvas_refresh",self,"synch")
		# drawing
		$right/rows/drawing/autolink.pressed = canvas.auto_link
		$right/rows/drawing/toogle.pressed = canvas.toggle_anchors
		$right/rows/drawing/overlaps.pressed = canvas.avoid_overlaps
# warning-ignore:return_value_discarded
		$right/rows/drawing/autolink.connect("pressed",self,"anchor_autolink")
# warning-ignore:return_value_discarded
		$right/rows/drawing/toogle.connect("pressed",self,"anchor_toogle")
# warning-ignore:return_value_discarded
		$right/rows/drawing/overlaps.connect("pressed",self,"anchor_overlaps")
		# visibility
		$right/rows/visible/guides.pressed = canvas.get_node('guides').visible
		$right/rows/visible/anchors.pressed = canvas.get_node('anchors').visible
# warning-ignore:return_value_discarded
		$right/rows/visible/guides.connect("pressed",self,"visible_guides")
# warning-ignore:return_value_discarded
		$right/rows/visible/anchors.connect("pressed",self,"visible_anchors")

	
	#### ANCHORS ####
	anchors_refresh()
	
	#### SEQUENCE ####
	sequence_refresh()
		
	$right.rect_size = Vector2.ZERO
	adjust_request = true

func anchors_refresh():
	
	# purge
	anchor_tmpls = []
	curly.purge( $right/rows/anchors )
	
	$right/anchor_link.visible = false
	
	if canvas != null:
		
		#### ANCHORS ####
		var anchors:Array = canvas.anchors
		var anum:int = anchors.size()
		var columns_x:Array = []
		# count columns of anchors
		for i in range(0,anum):
			var a:CheckBox = canvas.anchors[i]
			if !(a.rect_position.x in columns_x):
				columns_x.append( a.rect_position.x )
		columns_x.sort()
		$right/rows/anchors.columns = columns_x.size()
		var curr_col:int = -1
		for i in range(0,anum):
			var a:CheckBox = canvas.anchors[i]
			curr_col = (curr_col+1)%$right/rows/anchors.columns
			while( columns_x[curr_col] != a.rect_position.x ):
				curr_col = (curr_col+1)%$right/rows/anchors.columns
				var spacer:Label = Label.new()
				$right/rows/anchors.add_child(spacer)
			var lbl:Button = curly.anchor_button()
			$right/rows/anchors.add_child(lbl)
			anchor_tmpls.append(lbl)
			
			lbl.id = a.id
			anchor_clicked(a.id)
			# connecting signals
# warning-ignore:return_value_discarded
			lbl.connect( "button_anchor_clicked", self, "anchor_tmpl_clicked" )
# warning-ignore:return_value_discarded
			lbl.connect( "button_anchor_enter", self, "anchor_enter" )
# warning-ignore:return_value_discarded
			lbl.connect( "button_anchor_leave", self, "anchor_leave" )
# warning-ignore:return_value_discarded
			a.connect( "anchor_clicked", self, "anchor_clicked" )
# warning-ignore:return_value_discarded
			a.connect( "anchor_enter", self, "anchor_enter" )
# warning-ignore:return_value_discarded
			a.connect( "anchor_leave", self, "anchor_leave" )

func sequence_refresh():
	
	curly.purge( $right/rows/sequence )
		
	$right/sequence_link.visible = false
	
	if canvas == null:
		var lbl:Label = Label.new()
		lbl.text = 'load a character'
		$right/rows/sequence.add_child(lbl)
	
	else:
		
		if canvas.sequence.empty():
			var lbl:Label = Label.new()
			lbl.text = 'empty'
			$right/rows/sequence.add_child(lbl)
		else:
			var index:int = 0
			for s in canvas.sequence:
				var p:PanelContainer = selement_tmpl.duplicate()
				$right/rows/sequence.add_child(p)
				p.id = s.node.id
				p.index = index
				p.text( str(s.node.id) )
				p.link( s.connector != 0 )
				index += 1
	adjust_screen()

func anchor_autolink():
	canvas.auto_link = $right/rows/drawing/autolink.pressed

func anchor_toogle():
	canvas.toggle_anchors = $right/rows/drawing/toogle.pressed

func anchor_overlaps():
	canvas.avoid_overlaps = $right/rows/drawing/overlaps.pressed

func visible_guides():
	canvas.get_node('guides').visible = $right/rows/visible/guides.pressed

func visible_anchors():
	canvas.get_node('anchors').visible = $right/rows/visible/anchors.pressed

func anchor_tmpl_clicked(id:int):
	canvas.anchor_clear(id)
	anchor_clicked(id)

func anchor_clicked(id:int):
	curly.anchor_sync( canvas.anchors[id], anchor_tmpls[id] )
	sequence_refresh()

func anchor_enter(id:int):
	
	if !$right/rows/anchors.visible:
		return
	
	var lbl:Button = anchor_tmpls[id]
	var link:Line2D = $right/anchor_link
	var start:Vector2 = lbl.rect_global_position
	var target:Vector2 = canvas.anchor_position(id) - lbl.rect_global_position
	if target.y > 0:
		start.y += lbl.rect_size.y
		target.y -= lbl.rect_size.y
	var sub_target:Vector2 = target
	if target.x < 0:
		sub_target.x += 15
	else:
		sub_target.x -= 15
	if target.y < 0:
		sub_target.y += 15
	else:
		sub_target.y -= 15
	link.global_position = start
	link.clear_points()
	if target.y < 0:
		link.add_point(lbl.rect_size)
		link.add_point(Vector2(0,lbl.rect_size.y))
		link.add_point(Vector2.ZERO)
	else:
		link.add_point(lbl.rect_size * Vector2(1,-1))
		link.add_point(Vector2(0,-lbl.rect_size.y))
		link.add_point(Vector2.ZERO)
	
	if sub_target.x < 0 and sub_target.y < 0 and sub_target.y > sub_target.x:
		link.add_point(Vector2(sub_target.y,sub_target.y))
		link.add_point(sub_target)
	if sub_target.x < 0 and sub_target.y > 0 and -sub_target.y > sub_target.x:
		link.add_point(Vector2(-sub_target.y,sub_target.y) )
		link.add_point(sub_target)
	
	link.add_point(sub_target)
	link.add_point(target)
	link.visible = true

# warning-ignore:unused_argument
func anchor_leave(id:int):
	$right/anchor_link.visible = false

func button_panel_clicked(b:Button):
	var panel:Control = null
	if b == $right/rows/drawing_toggle:
		panel = $right/rows/drawing
	if b == $right/rows/visible_toggle:
		panel = $right/rows/visible
	elif b == $right/rows/anchors_toggle:
		panel = $right/rows/anchors
	elif b == $right/rows/sequence_toggle:
		panel = $right/rows/sequence
	if panel != null:
		panel.visible = !panel.visible
		if panel.visible:
			b.text = '- ' + b.text.substr(2)
		else:
			b.text = '+ ' + b.text.substr(2)
		$right.rect_size.y = 0
		adjust_screen()

func _ready():
	
# warning-ignore:return_value_discarded
	$right/rows/drawing_toggle.connect("button_panel_clicked", self, "button_panel_clicked")
# warning-ignore:return_value_discarded
	$right/rows/visible_toggle.connect("button_panel_clicked", self, "button_panel_clicked")
# warning-ignore:return_value_discarded
	$right/rows/anchors_toggle.connect("button_panel_clicked", self, "button_panel_clicked")
# warning-ignore:return_value_discarded
	$right/rows/sequence_toggle.connect("button_panel_clicked", self, "button_panel_clicked")
	button_panel_clicked($right/rows/drawing_toggle)
	button_panel_clicked($right/rows/visible_toggle)
	button_panel_clicked($right/rows/anchors_toggle)
	button_panel_clicked($right/rows/sequence_toggle)
	
	synch()
	if canvas != null:
		canvas.offset[1] += $right.rect_size.x
		canvas.center()
	adjust_screen()
# warning-ignore:return_value_discarded
	get_viewport().connect("size_changed",self,"adjust_screen")

# warning-ignore:unused_argument
func _process(delta):
	if adjust_request:
		adjust_screen()
		adjust_request = false

func _input(event):
	if canvas == null:
		return
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_SPACE:
			if self.visible:
				self.visible = false
				canvas.get_node('guides').visible = false
				canvas.get_node('anchors').visible = false
			else:
				self.visible = true
				visible_guides()
				visible_anchors()
