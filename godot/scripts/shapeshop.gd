extends Node2D

onready var circle:Line2D = 			$circle
onready var rectangle:Line2D = 			$rectangle
onready var triangle:Line2D = 			$triangle
onready var anchor:CheckBox = 			$control/anchor
onready var anchor_button:Button = 		$control/anchor_button
