extends Button

signal button_panel_clicked

func _ready():
# warning-ignore:return_value_discarded
	self.connect("pressed",self,"click")

func click():
	emit_signal( "button_panel_clicked", self )
